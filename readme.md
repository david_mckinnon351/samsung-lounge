# Read me
## Project structure
```
dist/
  |  css/
  |  |  main.min.css
  |  js/
  |  |  main.min.js
  |  fonts/
  |  |  CircularPro-Black
  |  |  CircularPro-BlackItalic
  |  |  CircularPro-Bold
  |  |  CircularPro-BoldItalic
  |  |  CircularPro-Book
  |  |  CircularPro-BookItalic
src/
  |  js/
  |  |  main.js
  |  |  wisdom.js
  |  |  power.js
  |  scss/
  |  |  _fonts.scss
  |  |  _scaffolding.scss
  |  |  _variables.scss
  |  |  main.scss
gulpfile.js
package.json
```
