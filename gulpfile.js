"use strict";

var gulp            = require('gulp'),
    autoprefixer    = require('gulp-autoprefixer'),
    bs              = require('browser-sync').create(),
    cssnano         = require('gulp-cssnano'),
    eslint          = require('gulp-eslint'),
    imagemin        = require('gulp-imagemin'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    rimraf          = require('rimraf');

gulp.task('clean', function (cb) {
    rimraf('./dist', cb);
});

gulp.task('html', function () {
  // copy any html files in src/ to dist/
    gulp.src('./src/*.html')
        .pipe(gulp.dest('./dist'));
});

gulp.task('fonts', function () {
  // copy any fonts in src/ to dist/fonts
    gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('images', function () {
    gulp.src('./src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img'));
});

gulp.task('styles', function () {
    gulp.src('./src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cssnano())
        .pipe(sourcemaps.write('./'))

        .pipe(gulp.dest('./dist/css/'))
        .pipe(bs.stream({once: true}));
});

gulp.task('lint', function () {
    gulp.src(['./src/js/*.js'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('js', function () {
    gulp.src('./src/js/*')
        //
        .pipe(gulp.dest('./dist/js'));
});

// Static Server + watching scss/html html
gulp.task('serve', ['styles'], function () {

    bs.init({
        server: "./dist"
    });

    bs.watch("./dist/**").on('change', bs.reload);

});

gulp.task('watch', function () {
    gulp.watch("./src/*.html", ['html']);
    gulp.watch("./src/scss/*.scss", ['styles']);
    gulp.watch("./src/js/*.js", ['js']);
    gulp.watch("./src/img/*", ['images']);
});

gulp.task('default', ['styles', 'html', 'images', 'watch', 'serve']);
